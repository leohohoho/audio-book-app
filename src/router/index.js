import Vue from 'vue'
import Router from 'vue-router'
import LogIn from '@/components/LogIn.vue'
import Register from '@/components/Register.vue'
import UserMenu from '@/components/UserMenu.vue'
import UserHomepage from '@/components/UserHomepage.vue'
import Player from '@/components/Player.vue'
import Account from '@/components/Account.vue'
import AdminMenu from '@/components/AdminMenu.vue'
import AddNewBook from '@/components/AddNewBook.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'LogIn',
      component: LogIn
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/usermenu',
      name: 'UserMenu',
      component: UserMenu
    },
    {
      path: '/userhomepage',
      name: 'UserHomepage',
      component: UserHomepage
    },
    {
      path: '/player',
      name: 'Player',
      component: Player
    },
    {
      path: '/account',
      name: 'Account',
      component: Account
    },
    {
      path: '/adminmenu',
      name: 'AdminMenu',
      component: AdminMenu
    },
    {
      path: '/addnewbook',
      name: 'AddNewBook',
      component: AddNewBook
    }
  ]
})
